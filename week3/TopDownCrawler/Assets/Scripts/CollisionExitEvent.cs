﻿using UnityEngine;
using System.Collections;
using System;

public class CollisionExitEvent : MonoBehaviour {

	public event Action<CollisionExitEvent, Collision2D> Triggered;

	void OnCollisionEnter2D(Collision2D other) {
		Debug.Log ("Enter");
	}

	void OnCollisionExit2D(Collision2D other) {
		var t = Triggered;
		if (t != null) {
			Triggered (this, other);
		}
	}
}
