﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class AcquireCamera : NetworkBehaviour {

	void Start () {
		if (isLocalPlayer) {
			var follow = Camera.main.GetComponent<FollowPlayer> ();
			if (follow != null) {
				follow.Target = this;
			}
		}
	}
}
