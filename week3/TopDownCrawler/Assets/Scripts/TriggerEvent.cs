﻿using UnityEngine;
using System;

public class TriggerEvent : MonoBehaviour {

	public event Action<Collider2D> TriggeredEnter;
	public event Action<Collider2D> TriggeredExit;

	void OnTriggerEnter2D(Collider2D other) {
		// Destroy everything that leaves the trigger
		var t = TriggeredEnter;
		if (t != null) {
			t (other);
		}
		Debug.Log("trigger enter");
	}

	void OnTriggerExit2D(Collider2D other) {
		// Destroy everything that leaves the trigger
		var t = TriggeredExit;
		if (t != null) {
			t (other);
		}
		Debug.Log("trigger exit");
	}
}
