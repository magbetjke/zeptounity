﻿using UnityEngine;
using System.Collections;

public class Die : MonoBehaviour {

	private MovePlayer move;
	private Animator animator;
	// Use this for initialization
	void Start () {
		var triggerEvent = GetComponent<TriggerEvent> ();
		if (triggerEvent != null) {
			triggerEvent.TriggeredExit += OnTriggeredSomething;
		}

		move = GetComponent<MovePlayer> ();
		animator = GetComponent<Animator> ();
	}

	void Kill() {
		if (move != null) {
			move.enabled = false;
		}

		if (animator != null) {
			animator.Play ("Dead");
		}
	}

	void OnTriggeredSomething (Collider2D collider)
	{
		if (collider.tag == "Bounds") {
			Kill ();
		}
	}
}
