﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

//	[SerializeField]
	public MonoBehaviour Target;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Target) {
			var targetPosition = Target.transform.position;
			var newPosition = transform.position;
			newPosition.x = targetPosition.x;
			newPosition.y = targetPosition.y;
			transform.position = newPosition;
		}
	}
}
