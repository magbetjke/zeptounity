﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MovePlayer : NetworkBehaviour {
	[Tooltip("Units per second")]
	[SerializeField]
	private float MoveSpeed = 1.0f;

	private Animator animator;
	private Camera cam;

	private Vector3 moveTo;
	private Vector3 moveFrom;
	private float moveDuration = 0.0f;
	private float timeSpent = 0.0f;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer)
			return;

		if (Input.GetMouseButtonDown(0)) {
			var point = Input.mousePosition;

			moveTo = cam.ScreenToWorldPoint (point);
			moveFrom = transform.position;
			moveTo.z = moveFrom.z;
			var direction = moveTo - moveFrom;
			moveDuration = direction.magnitude / MoveSpeed;
			timeSpent = moveDuration;
			direction.Normalize ();
			var angle = Quaternion.FromToRotation(Vector3.up, direction).eulerAngles.z;
			transform.rotation = Quaternion.Euler (0.0f, 0.0f, angle);

			if (animator != null) {
				animator.Play ("Walk");
			}

		}

		if (timeSpent > 0.0f) {
			var currentPosition = transform.position;
			var newPosition = Vector3.Lerp (moveFrom, moveTo, 1.0f - (timeSpent / moveDuration));
			currentPosition.x = newPosition.x;
			currentPosition.y = newPosition.y;
			transform.position = currentPosition;
			timeSpent -= Time.deltaTime;

			if (timeSpent <= 0.0f && animator != null) {
				animator.Play ("Idle");
			}
		}
	}
}
