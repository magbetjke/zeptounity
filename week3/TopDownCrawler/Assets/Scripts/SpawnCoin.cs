﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Assertions;

public class SpawnCoin : NetworkBehaviour {

	[SerializeField]
	private float MaxSpawnOffset = 4.0f;
	[SerializeField]
	private float MinSpawnOffset = 1.0f;

	[SerializeField]
	private GameObject coinPrefab;

	void Start () {
		if (isServer) {
			var triggerEvent = GetComponent<TriggerEvent> ();
			if (triggerEvent != null) {
				triggerEvent.TriggeredEnter += OnTriggeredEnter;
			}
		}
	}

	float GetRandomOffset() {
		return (Random.value > 0.5f ? -1 : 1) * Random.Range (MinSpawnOffset, MaxSpawnOffset);
	}

	[Command]
	void CmdSpawnNew() {
		Assert.IsTrue (isServer);

		var position = transform.position;
		position.x += GetRandomOffset();
		position.y += GetRandomOffset();
		var coin = Instantiate (coinPrefab, position, Quaternion.identity) as GameObject;

		NetworkServer.Spawn (coin);
	}

	[Command]
	void CmdKillCurrent() {
		Assert.IsTrue (isServer);

		NetworkServer.Destroy (gameObject);
	}

	void OnTriggeredEnter (Collider2D collider)
	{
		if (collider.tag == "Player") {
			GetComponent<TriggerEvent> ().TriggeredEnter -= OnTriggeredEnter;
			CmdSpawnNew ();
			CmdKillCurrent ();
		}
	}
}
