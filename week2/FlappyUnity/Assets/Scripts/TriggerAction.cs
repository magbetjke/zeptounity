﻿using UnityEngine;
using System;

public class TriggerAction : MonoBehaviour 
{
	public event Action<TriggerAction> Triggered;

	void OnTriggerEnter2D (Collider2D other)
	{
		var t = Triggered;
		if (t != null && !other.isTrigger)
			Triggered (this);
		Debug.Log ("Triggered");
	}
}

