﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {

	[SerializeField]
	private int maxPipes = 2;

	[SerializeField]
	private float distance = 10.0f;

	[SerializeField]
	private float speed = 1.0f;

	private Text scoreLabel;
	private int score = 0;

	[SerializeField]
	private float maxVerticalOffset = 1.0f;

	[SerializeField]
	private Vector2 direction = new Vector2(-1.0f, 0.0f);

	[SerializeField]
	private GameObject pipePrefab;

	private List<GameObject> pipes;
	private Camera cam;

	private bool _running = true;
	public bool runinng {
		get { return _running; }
	}

	// Use this for initialization
	void Start () {
		cam = Camera.main;

		var scoreObject = GameObject.FindWithTag ("ScoreLabel");
		if (scoreObject != null) {
			scoreLabel = scoreObject.GetComponent<Text> ();
		}

		pipes = new List<GameObject> ();
		for (var i = 0; i != maxPipes; ++i) {
			var pipe = Instantiate (pipePrefab, new Vector3(5.0f + i * distance, 0.0f, 0.0f), Quaternion.identity) as GameObject;
			pipes.Add(pipe);
			pipe.transform.parent = transform;
			pipe.transform.localScale = Vector3.one;

			var triggered = pipe.GetComponentInChildren<TriggerAction> ();
			if (triggered != null) {
				triggered.Triggered += OnPipeTriggered;
			}
		}

		var bird = GameObject.FindWithTag ("Bird");
		var birdTriggered = bird.GetComponent<TriggerAction> ();
		if (birdTriggered != null) {
			birdTriggered.Triggered += OnBirdTriggered;
		}
	}

	void OnPipeTriggered (TriggerAction obj)
	{
		++score;
		if (scoreLabel != null) {
			scoreLabel.text = score.ToString();
		}
	}

	void OnBirdTriggered (TriggerAction obj)
	{
		_running = false;
		Invoke("GameOver", 3);
	}

	private void GameOver()
	{
		SceneManager.LoadScene (0);
	}

	// Update is called once per frame
	void Update () {
		if (!_running)
			return;

		var size = pipes.Count;
		for (var i = 0; i != size; ++i) {
			var pipe = pipes [i];
			var position = pipe.transform.position;
			var viewPosition = cam.WorldToViewportPoint(position);
			if (viewPosition.x <= 0) {
				Spawn (i);
			} else {
				var delta = Time.deltaTime * speed;
				position.x += delta * direction.x;
				position.y += delta * direction.y;
				pipe.transform.position = position;
			}
		}
	}

	void Spawn(int index)
	{
		if (index >= 0 && pipes.Count > index) {
			
			var prevIndex = (index == 0 ? pipes.Count - 1 : index - 1);
			var prevPipe = pipes [prevIndex];
			var currentPipe = pipes [index];

			var position = currentPipe.transform.position;
			var prevPipePosition = prevPipe.transform.position;

			var halfOffset = maxVerticalOffset * 0.5f;
			position.x = prevPipePosition.x + distance;
			position.y = prevPipePosition.y + Random.Range (-halfOffset, halfOffset);

			currentPipe.transform.position = position;
		}
	}
}
