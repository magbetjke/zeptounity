﻿using UnityEngine;
using System.Collections;

public class MenuBird : MonoBehaviour {

	[SerializeField]
	private float amplitude;
	
	// Update is called once per frame
	void Update () {
		var nextPosition = transform.position;
		nextPosition.y = Mathf.Sin(Time.time) * amplitude;
		transform.position = nextPosition;
	}
}
