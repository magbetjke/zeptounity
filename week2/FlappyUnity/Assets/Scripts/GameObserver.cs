﻿using UnityEngine;
using System.Collections;

public class GameObserver : MonoBehaviour {

	private Game gameController;

	public bool running {
		get { return gameController ? gameController.runinng : true; }
	}

	// Use this for initialization
	void Start () {
		var gameControllerHolder = GameObject.FindWithTag ("Main");
		if (gameControllerHolder) {
			gameController = gameControllerHolder.GetComponent<Game> ();
		}
	}
}
