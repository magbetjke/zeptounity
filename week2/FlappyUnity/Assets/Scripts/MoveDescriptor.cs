﻿using UnityEngine;
using System.Collections;

public class MoveDescriptor : MonoBehaviour {

	[SerializeField]
	private float speed = 1.0f;
	public float Speed {
		get { return config ? config.Speed : speed; }
	}

	[SerializeField]
	private Vector2 direction = new Vector2(-1.0f, 0.0f);
	public Vector2 Direction {
		get { return direction; }
	}

	private Config config;

	// Use this for initialization
	void Start () {
		var configHolder = GameObject.Find ("Config");
		if (configHolder) {
			config = configHolder.GetComponent<Config> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
