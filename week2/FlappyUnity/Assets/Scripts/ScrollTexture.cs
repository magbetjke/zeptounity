﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveDescriptor))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(GameObserver))]
public class ScrollTexture : MonoBehaviour {

	private MoveDescriptor move;
	private Mesh mesh;
	private float offset;

	private Config config;
	private GameObserver game;

	// Use this for initialization
	void Start () {
		move = GetComponent<MoveDescriptor> ();
		mesh = GetComponent<MeshFilter>().mesh;
		game = GetComponent<GameObserver> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!game.running)
			return;
		
		UpdateUVs ();
	}

	void UpdateUVs()
	{
		var uvs = mesh.uv;
		for (int index = 0; index != uvs.Length; ++index)
		{
			uvs [index] += move.Direction * move.Speed * Time.deltaTime;
		}

		mesh.uv = uvs;
	}
}
