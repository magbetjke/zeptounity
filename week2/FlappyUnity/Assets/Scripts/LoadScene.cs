﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

	[SerializeField]
	private int index;

	public void Load() {
		SceneManager.LoadScene (index);
	}
}
