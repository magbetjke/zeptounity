﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(GameObserver))]
public class BirdController : MonoBehaviour {

	[SerializeField]
	private float force = 1.0f;
	private GameObserver game;

	private Rigidbody2D rigidBody;
	private Animator animator;

	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody2D> ();
		game = GetComponent<GameObserver> ();
		animator = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (!game.running) {
			if (animator) //dunno how to check if animator is playing
				animator.Stop ();
			return;
		}
			

		if (Input.GetMouseButtonDown(0)) {
			rigidBody.AddForce (new Vector2 (0.0f, force), ForceMode2D.Impulse);
		}
	}
}
